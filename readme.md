minikube start
minikube addons enable ingress
minikube -p minikube docker-env
minikube -p minikube docker-env --shell powershell | Invoke-Expression
`docker build -t docker.io/library/employee-service:0.0.1-SNAPSHOT .`
kubectl apply -f deployment.yaml


http://first.bar.com/api/v1/employees