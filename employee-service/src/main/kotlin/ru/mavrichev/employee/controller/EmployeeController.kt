package ru.mavrichev.employee.controller

import org.springframework.web.bind.annotation.*
import ru.mavrichev.employee.domain.Employee
import ru.mavrichev.employee.dto.EmployeeDto
import ru.mavrichev.employee.service.EmployeeService
import java.util.UUID

@RequestMapping("/api/v1/employees")
@RestController
class EmployeeController(
        private val employeeService: EmployeeService
) {
    @GetMapping
    fun get(): List<EmployeeDto> {
        return employeeService.listAll()
    }

    @GetMapping("/{employeeId}")
    fun get(@PathVariable employeeId: UUID): EmployeeDto? {
        return employeeService.getById(employeeId)
    }

    @GetMapping("/")
    fun dmma(): String {
        return "Property = " + System.getenv("TEST")
    }
}