package ru.mavrichev.employee.service

import mu.KLogging
import org.springframework.stereotype.Service
import ru.mavrichev.employee.domain.Employee
import ru.mavrichev.employee.dto.EmployeeDto
import ru.mavrichev.employee.mapper.Mapper
import ru.mavrichev.employee.repository.EmployeeRepository
import java.util.*

interface EmployeeService {
    fun listAll(): List<EmployeeDto>
    fun getById(employeeId: UUID): EmployeeDto?
}

@Service
class EmployeeServiceImpl(
        private val employeeRepository: EmployeeRepository
): EmployeeService {

    val mapper = Mapper()

    override fun listAll(): List<EmployeeDto> {
        logger.info { "Fetching all employees" }
        val findAll = employeeRepository.findAll()
        return findAll.map { EmployeeDto(it.id, it.name, mapper.dmma(it.departmentId)) }
    }

    override fun getById(employeeId: UUID): EmployeeDto? {
        logger.info { "Fetching employee ny id" }
        val findByID = employeeRepository.findByID(employeeId)!!
        return EmployeeDto(findByID.id, findByID.name, mapper.dmma(findByID.departmentId))
    }

    companion object: KLogging()
}