package ru.mavrichev.employee.dto

import java.util.*

class DepartmentDto(
        val id: UUID,
        val name: String
)