package ru.mavrichev.employee.dto

import ru.mavrichev.employee.domain.Employee
import java.util.*

class EmployeeDto(
        val id: UUID,
        val name: String,
        val department: DepartmentDto
)