package ru.mavrichev.employee.domain

import java.util.UUID

class Employee(
        val id: UUID,
        val name: String,
        val departmentId: UUID
)