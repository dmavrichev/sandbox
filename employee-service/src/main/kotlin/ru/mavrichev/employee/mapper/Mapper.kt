package ru.mavrichev.employee.mapper

import org.springframework.web.client.RestTemplate
import ru.mavrichev.employee.dto.DepartmentDto
import java.util.UUID

class Mapper {

    val client = RestTemplate()

    fun dmma(departmentId: UUID): DepartmentDto {
        //http://localhost:8081/api/v1/department/331160b6-880c-45fe-a1f5-2fd55ac065c4
        val forEntity = client.getForObject("http://localhost:8081/api/v1/department/{id}", DepartmentDto::class.java, departmentId)
        return DepartmentDto(UUID.randomUUID(), "Test") //forEntity!!
    }
}