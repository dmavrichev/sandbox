package ru.mavrichev.employee.repository

import org.springframework.stereotype.Repository
import ru.mavrichev.employee.domain.Employee
import java.util.*

@Repository
class EmployeeRepository {
    val store: MutableList<Employee>
        get() = mutableListOf(
                Employee(UUID.fromString("8a87cc6a-d822-4853-b4fd-addfbb8f0437"), "Dmitry", UUID.fromString("331160b6-880c-45fe-a1f5-2fd55ac065c4")),
                Employee(UUID.fromString("56cd8876-560e-4184-82b5-80f50521c9a2"), "Klaudia", UUID.fromString("c233ddfa-1a94-4fc8-91b7-6f7764b68ae9"))
        )

    fun findAll() = store

    fun findByID(id: UUID) = store.firstOrNull { employee -> employee.id == id }
}