plugins {
	id("org.springframework.boot") version "3.0.5"
	id("io.spring.dependency-management") version "1.1.0"
	kotlin("jvm") version "1.7.22"
	kotlin("plugin.spring") version "1.7.22"
}

group = "ru.mavrichev"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_17

//subprojects {
//
//	group = 'io.reflectoring.reviewapp'
//	version = '0.0.1-SNAPSHOT'
//
//	apply plugin: 'java'
//	apply plugin: 'io.spring.dependency-management'
//	apply plugin: 'java-library'
//
//	repositories {
//		jcenter()
//	}
//
//	dependencyManagement {
//		imports {
//			mavenBom("org.springframework.boot:spring-boot-dependencies:2.1.7.RELEASE")
//		}
//	}
//
//}

repositories {
	mavenCentral()
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	developmentOnly("org.springframework.boot:spring-boot-devtools")
	testImplementation("org.springframework.boot:spring-boot-starter-test")

	implementation("io.github.microutils:kotlin-logging:3.0.5")
}

tasks.compileKotlin {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "17"
	}
}

tasks.bootBuildImage {
//	imageName = "docker.io/ringo"
//	createdDate = "now"
}

tasks.test {
	useJUnitPlatform()
}
