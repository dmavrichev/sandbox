package ru.mavrichev.department.controller

import org.springframework.web.bind.annotation.*
import ru.mavrichev.department.dto.DepartmentDto
import ru.mavrichev.department.service.DepartmentService
import tech.ydb.auth.iam.CloudAuthHelper
import tech.ydb.core.grpc.GrpcTransport
import tech.ydb.table.SessionRetryContext
import tech.ydb.table.TableClient
import tech.ydb.table.query.DataQueryResult
import tech.ydb.table.result.ResultSetReader
import tech.ydb.table.transaction.TxControl
import java.nio.file.Paths
import java.util.*
import kotlin.io.path.pathString
import kotlin.time.Duration.Companion.seconds
import kotlin.time.toJavaDuration


@RequestMapping("/api/v1/department")
@RestController
class DepartmentController(
    val departmentService: DepartmentService
) {
    @GetMapping
    fun get(): List<DepartmentDto> {
        return departmentService.listAll()
    }



    @GetMapping("/{departmentId}")
    fun get(@PathVariable departmentId: Long): DepartmentDto? {
        val byId = departmentService.getById(departmentId)!!
        return DepartmentDto(byId.id, byId.name)
    }
}