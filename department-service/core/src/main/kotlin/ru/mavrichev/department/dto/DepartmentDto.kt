package ru.mavrichev.department.dto

import java.util.*

data class DepartmentDto(
    val id: Long,
    val name: String
)