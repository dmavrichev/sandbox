package ru.mavrichev.department

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cache.annotation.EnableCaching

@SpringBootApplication
@EnableCaching
class DepartmentApplication

fun main(args: Array<String>) {
        runApplication<DepartmentApplication>(*args)
}
