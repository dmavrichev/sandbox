//package ru.mavrichev.department.rest
//
//import tech.ydb.auth.iam.CloudAuthHelper
//import tech.ydb.core.grpc.GrpcTransport
//import tech.ydb.table.TableClient
//import java.nio.file.Paths
//import kotlin.io.path.pathString
//
//class YdbClient {
//    val sa = Paths.get(System.getProperty("user.home") + "/.ydb/prime-sa.json")
//    val authProvider = CloudAuthHelper.getServiceAccountFileAuthProvider(sa.pathString)
//    val transport: GrpcTransport = GrpcTransport.forEndpoint(
//        s,
//        "/ru-central1/b1g9ltptgljrgmbmpgnd/etn5tl33i38h8senhv1f"
//    )
//        .withAuthProvider(authProvider)
//        .withSecureConnection()
//        .build()
//
//    val tableClient: TableClient = TableClient.newClient(transport)
//        .keepQueryText(true)
//        .sessionPoolSize(1, 5)
//        .build()
//}