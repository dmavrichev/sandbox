package ru.mavrichev.department.rest

import mu.KLogging
import org.springframework.cache.annotation.Cacheable
import org.springframework.stereotype.Service
import ru.mavrichev.department.dto.DepartmentDto
import tech.ydb.auth.iam.CloudAuthHelper
import tech.ydb.core.grpc.GrpcTransport
import tech.ydb.table.SessionRetryContext
import tech.ydb.table.TableClient
import tech.ydb.table.query.Params
import tech.ydb.table.result.ResultSetReader
import tech.ydb.table.transaction.TxControl
import tech.ydb.table.values.PrimitiveValue
import java.nio.file.Paths
import kotlin.io.path.pathString


private const val s = "ydb.serverless.yandexcloud.net:2135"

//@Cacheable("departments", key = "#root.id")
@Cacheable("departments")
@Service
class YdbAdapter {
    private val retryCtx: SessionRetryContext = run {
        val sa = Paths.get(System.getProperty("user.home") + "/.ydb/prime-sa.json")
        val authProvider = CloudAuthHelper.getServiceAccountFileAuthProvider(sa.pathString)

        val transport: GrpcTransport =
            GrpcTransport.forEndpoint(s, "/ru-central1/b1g9ltptgljrgmbmpgnd/etn5tl33i38h8senhv1f")
                .withAuthProvider(authProvider)
                .withSecureConnection()
                .build()

        val tableClient: TableClient = TableClient.newClient(transport)
            .keepQueryText(true)
            .sessionPoolSize(1, 5)
            .build()

        SessionRetryContext.create(tableClient).build()
    }

    fun list(): List<DepartmentDto> = retryCtx.supplyResult { session ->
        logger.debug { "List all" }
        val txControl: TxControl<*> = TxControl.serializableRw().setCommitTx(true)
        session.executeDataQuery("select id, name from table801", txControl)
    }.thenApply { result ->
        val rs = result.value.getResultSet(0)
        buildList {
            while (rs.next()) {
                add(departmentDto(rs))
            }
        }
    }.join()

    fun get(departmentId: Long): DepartmentDto = retryCtx.supplyResult { session ->
        logger.debug { "Get by id $departmentId" }
        val txControl: TxControl<*> = TxControl.serializableRw().setCommitTx(true)
        val params = Params.of("\$departmentId", PrimitiveValue.newUint64(departmentId))
        session.executeDataQuery("DECLARE \$departmentId AS Uint64;\n" +
                "select id, name from table801 where id = \$departmentId", txControl, params)
    }.thenApply { result ->
        val rs = result.value.getResultSet(0)
        rs.next()
        departmentDto(rs)
    }.join()

    private fun departmentDto(resultSet: ResultSetReader) = DepartmentDto(
        id = resultSet.getColumn(0).uint64,
        name = resultSet.getColumn(1).bytes.decodeToString()
    )

    companion object: KLogging()
}