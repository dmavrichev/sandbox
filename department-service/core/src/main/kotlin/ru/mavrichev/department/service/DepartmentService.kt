package ru.mavrichev.department.service

import mu.KLogging
import org.springframework.stereotype.Service
import ru.mavrichev.department.domain.Department
import ru.mavrichev.department.dto.DepartmentDto
import ru.mavrichev.department.rest.YdbAdapter
import java.util.*

interface DepartmentService {
    fun listAll(): List<DepartmentDto>
    fun getById(departmentId: Long): DepartmentDto?
}

@Service
class DepartmentServiceImpl(private val ydbAdapter: YdbAdapter): DepartmentService {
    val store = mutableListOf(
            Department(UUID.fromString("c233ddfa-1a94-4fc8-91b7-6f7764b68ae9"), "Accounting"),
            Department(UUID.fromString("331160b6-880c-45fe-a1f5-2fd55ac065c4"), "IT")
    )

//    val ydbAdapter = ()

    override fun listAll(): List<DepartmentDto> = ydbAdapter.list()

    override fun getById(departmentId: Long): DepartmentDto? = ydbAdapter.get(departmentId)

    companion object: KLogging()
}