package ru.mavrichev.department.domain

import java.util.UUID

class Department(
        val id: UUID,
        val name: String
)