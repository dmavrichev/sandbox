package ru.mavrichev.department.dto

import java.util.*

class DepartmentDto(
        val id: UUID,
        val name: String
)